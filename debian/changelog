pelican (3.7.1+dfsg-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Remove ancient X-Python-Version field

  [ Geert Stappers ]
  * d/control has Salsa in VCS fields. (Closes: #917468)
  * Build depend on python3-feedgenerator

 -- Geert Stappers <stappers@debian.org>  Sat, 05 Jan 2019 15:27:25 +0100

pelican (3.7.1+dfsg-1) unstable; urgency=medium

  * Remove notmyidea theme to avoid shipping non-free files, repacking tarball.
    (Closes: #868047, #868049, #858859)
    - Update debian/copyright and debian/lintian-overrides accordingly.
    - Add debian/patches/default_theme.patch to change default theme to
      "simple".
  * Update Standards version to 4.0.1.
    - Change python-pelican priority due to "extra" being deprecated.

 -- Vincent Cheng <vcheng@debian.org>  Sat, 12 Aug 2017 23:01:00 -0700

pelican (3.7.1-1) unstable; urgency=medium

  * New upstream release.
    - Drop quickstart_attribute_error.patch; applied upstream.

 -- Vincent Cheng <vcheng@debian.org>  Wed, 11 Jan 2017 00:16:54 -0800

pelican (3.7.0-2) unstable; urgency=medium

  * Update debian/patches/quickstart_attribute_error.patch with upstream fix.

 -- Vincent Cheng <vcheng@debian.org>  Thu, 05 Jan 2017 01:33:02 -0800

pelican (3.7.0-1) unstable; urgency=medium

  * New upstream release. (Closes: #849583)
  * Add debian/patches/quickstart_attribute_error.patch to fix AttributeError
    when invoking pelican-quickstart.
  * Update dependency on python-feedgenerator to >= 1.9.
  * Update Standards version to 3.9.8, no changes required.
  * Update dh compat level to 9.

 -- Vincent Cheng <vcheng@debian.org>  Mon, 02 Jan 2017 12:43:00 -0800

pelican (3.6.3-1) unstable; urgency=medium

  * New upstream release.

 -- Vincent Cheng <vcheng@debian.org>  Fri, 14 Aug 2015 23:04:05 -0700

pelican (3.6.2-1) unstable; urgency=medium

  * New upstream release.
  * Change Section field in d/control from python to web. (Closes: #791386)
  * Add dependency “Suggests: pelican-doc” to pelican. (Closes: #791387)

 -- Vincent Cheng <vcheng@debian.org>  Mon, 03 Aug 2015 13:18:50 -0700

pelican (3.6.0-4) unstable; urgency=medium

  * Build pelican reproducibly. (Closes: #790309)

 -- Vincent Cheng <vcheng@debian.org>  Sun, 28 Jun 2015 21:03:35 -0700

pelican (3.6.0-3) unstable; urgency=medium

  * Fix broken package upgrades by declaring Breaks/Replaces on correct
    version. (Closes: #790052)

 -- Vincent Cheng <vcheng@debian.org>  Sat, 27 Jun 2015 01:35:17 -0700

pelican (3.6.0-2) unstable; urgency=medium

  * Rename binary package from python-pelican -> pelican.
  * Build documentation and install into new pelican-doc package. Thanks to
    Federico Ceratto for the patch! (Closes: #754071)
    - Add build-depends on python-sphinx, dh-python, in addition to existing
      python module dependencies for sphinx to build docs from source.

 -- Vincent Cheng <vcheng@debian.org>  Tue, 16 Jun 2015 22:27:08 -0700

pelican (3.6.0-1) unstable; urgency=medium

  * New upstream release.
    - Fix a datetime comparison error related to time zones. (Closes: #787591)
  * Update debian/watch.

 -- Vincent Cheng <vcheng@debian.org>  Mon, 15 Jun 2015 22:30:19 -0700

pelican (3.5.0-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards version to 3.9.6, no changes required.

 -- Vincent Cheng <vcheng@debian.org>  Wed, 05 Nov 2014 23:51:52 -0800

pelican (3.4.0-2) unstable; urgency=medium

  * Fix broken help2man-generated manpages by manually generating them; this
    avoids an unwanted build-dependency on python-pelican itself.
    (Closes: #754612)
  * Remove references to asciidoc support in package description since this
    functionality was moved out of pelican core into a plugin.
    (Closes: #754153)

 -- Vincent Cheng <vcheng@debian.org>  Mon, 04 Aug 2014 01:22:11 -0700

pelican (3.4.0-1) unstable; urgency=medium

  * New upstream release.

 -- Vincent Cheng <vcheng@debian.org>  Tue, 01 Jul 2014 16:51:36 -0700

pelican (3.3-2) unstable; urgency=medium

  * Add dependency on python-pkg-resources. (Closes: #744100)
  * Add python-bs4 and pandoc to Suggests. (Closes: #742435)
  * Generate man pages using help2man.
    - Add build-dependency on help2man.
  * Add lintian override for privacy-breach-google-adsense,
    privacy-breach-piwik, and privacy-breach-twitter. Rationale:
    - These are only sample template files distributed with the default theme
      used by Pelican (notmyidea). None of these are enabled by default, and
      as these HTML files are actually nothing more than jinja snippets,
      opening them in a browser is harmless (no javascript code gets executed).

 -- Vincent Cheng <vcheng@debian.org>  Sun, 20 Apr 2014 02:27:01 -0700

pelican (3.3-1) unstable; urgency=medium

  * Team upload.
  * Add myself to Uploaders.
  * New upstream release. (Closes: #725340)
  * Add python-typogrify as dependency.
  * Update Standards version to 3.9.5.
  * Add missing Vcs-* fields in debian/control.

 -- Vincent Cheng <vcheng@debian.org>  Wed, 22 Jan 2014 23:43:44 -0800

pelican (3.2.2-2) unstable; urgency=low

  * Add Homepage to debian/control (Closes: #722352)
  * Move the package under Debian Python Applications Team

 -- Ondřej Surý <ondrej@debian.org>  Tue, 10 Sep 2013 16:16:04 +0200

pelican (3.2.2-1) unstable; urgency=low

  * New upstream version 3.2.2 (Closes: #711875)
  * Add hard dependency on python-markdown (Closes: #714781)

 -- Ondřej Surý <ondrej@debian.org>  Mon, 05 Aug 2013 13:25:38 +0200

pelican (3.2.1-1) unstable; urgency=low

  * Initial packaging (Closes: #702222)

 -- Ondřej Surý <ondrej@debian.org>  Mon, 04 Mar 2013 11:31:08 +0100
