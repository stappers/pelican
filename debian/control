Source: pelican
Section: web
Priority: optional
Maintainer: Debian Python Applications Team <python-apps-team@lists.alioth.debian.org>
Uploaders: Ondřej Surý <ondrej@debian.org>, Vincent Cheng <vcheng@debian.org>
Build-Depends:
 debhelper (>= 9),
 dh-python,
 python-all (>= 2.6.6-3),
 python-blinker,
 python-dateutil,
 python-docutils,
 python3-feedgenerator,
 python-jinja2 (>= 2.7),
 python-pygments,
 python-setuptools (>= 0.6b3),
 python-six (>= 1.4),
 python-sphinx,
 python-tz,
 python-unidecode
Standards-Version: 4.0.1
Homepage: http://getpelican.com/
Vcs-Git: https://salsa.debian.org/python-team/applications/pelican.git
Vcs-Browser: https://salsa.debian.org/python-team/applications/pelican

Package: pelican
Architecture: all
Depends:
 python-argparse | python (>= 2.7),
 python-markdown,
 python-pkg-resources,
 ${misc:Depends},
 ${python:Depends}
Suggests:
 pandoc,
 python-bs4,
 pelican-doc
Breaks:
 python-pelican (<< 3.6.0-2~)
Replaces:
 python-pelican (<< 3.6.0-2~)
Description: blog aware, static website generator
 Pelican is a static site generator, written in Python.  It allows you
 to write your weblog entries directly with your editor of choice in
 reStructuredText or Markdown, and generates completely static output
 that is easy to host anywhere.  Pelican includes a simple CLI tool to
 (re)generate the weblog and it is easy to interface with DVCSes and web
 hooks.

Package: pelican-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${python:Depends},
 ${sphinxdoc:Depends}
Suggests:
 pelican
Description: blog aware, static website generator (documentation)
 Pelican is a static site generator, written in Python.  It allows you
 to write your weblog entries directly with your editor of choice in
 reStructuredText or Markdown, and generates completely static output
 that is easy to host anywhere.  Pelican includes a simple CLI tool to
 (re)generate the weblog and it is easy to interface with DVCSes and web
 hooks.
 .
 This package provides documentation for Pelican.

Package: python-pelican
Architecture: all
Section: oldlibs
Depends:
 pelican,
 ${misc:Depends}
Description: transitional dummy package
 This is a transitional dummy package. It can safely be removed.
